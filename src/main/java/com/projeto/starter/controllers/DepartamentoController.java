package com.projeto.starter.controllers;

import java.util.List;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.starter.exceptions.NotFoundException;
import com.projeto.starter.models.Departamento;
import com.projeto.starter.models.Funcionario;
import com.projeto.starter.repository.DepartamentoRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api", produces = "application/json")
@Api(value = "departamento")
public class DepartamentoController {
	
	/*
	 * @RestController -> This annotation is a combination of Spring’s @Controller and @ResponseBody annotations.
	 * @RequestMapping -> anotação para mapear requisições rest
	 * @Api -> marca a classe como um recurso do swagger
	 * @Autowired -> avisa ao Spring Framework para injetar uma instância de alguma implementação da interface
	 * @ApiOperation -> descreve o método no swagger-ui
	 * @GetMapin -> mapeia requisições get
	 * @PutMapping ->
	 * @PostMapping ->
	 * @DeleteMapping ->
	 * @RequestBody -> anotação que indica a necesside de se passar os parâmetros de método no corpo da requisição
	 * @Valid -> the annotation makes sure that the request body is valid
	 * */
	
	//private final Logger logger = LoggerFactory.getLogger(DepartamentoController.class);

	@Autowired
	DepartamentoRepository departamentoRepository;

	@GetMapping("/departamentos")
	@ApiOperation(value = "Lista todos os departamentos")
	public List<Departamento> getAllDepartamentos() {

		List<Departamento> departamento = departamentoRepository.findAll();
		
		if (departamento.isEmpty()) {
			throw new NotFoundException("sem cadastros");
		}
		
		return departamento;
	}

	@GetMapping("/departamentos/cr/{cr}")
	@ApiOperation(value = "Busca o departamento pelo CR")
	public Departamento getDepartamentoByCr(@PathVariable(value = "cr") String cr) {

		Departamento departamento = null;

		try {
			departamento = departamentoRepository.findById(cr).get();
			
		} catch (NoSuchElementException nsee) {
			throw new NotFoundException("cr = " + cr);
		}

		return departamento;
	}

	@GetMapping("/departamentos/nome/{nome}")
	@ApiOperation(value = "Lista os departamentos pelo nome")
	public List<Departamento> getDepartamentoByName(@PathVariable(value = "nome") String nome) {
		// logger.info(nome);
		List<Departamento> departamento = departamentoRepository.findByName(nome);

		if (departamento.isEmpty()) {
			throw new NotFoundException("nome = " + nome);
		}

		return departamento;
	}
	
	@PutMapping("/departamentos/cr/{cr}")
	@ApiOperation(value = "Atualiza o departamento especificado pelo CR")
	public Departamento updateDepartamento(@Valid @RequestBody Departamento departamento,
			@PathVariable(value = "cr") String cr) {

		Departamento depto = null;

		try {
			depto = departamentoRepository.findById(cr).get();

			depto.setNome(departamento.getNome());
			depto.setMatriculaCoordenador(departamento.getMatriculaCoordenador());

			depto = departamentoRepository.save(depto);

		} catch (NoSuchElementException nsee) {
			throw new NotFoundException("cr = " + cr);
		}

		return depto;
	}

	@PostMapping("/departamentos")
	@ApiOperation(value = "Adiciona um departamento")
	public Departamento addDepartamento(@Valid @RequestBody Departamento departamento) {
		return departamentoRepository.save(departamento);
	}
	/*
	 * The @RequestBody annotation is used to bind the request body with a method
	 * parameter. The @Valid annotation makes sure that the request body is valid.
	 * Remember, we had marked Departamento's nome and content with
	 * 
	 * @NotBlank annotation in the Departamento model?
	 */

	@DeleteMapping("/departamentos/cr/{cr}")
	@ApiOperation(value = "Deleta o departamento especificado pelo CR")
	public Departamento deleteDepartamento(@PathVariable(value = "cr") String cr) {

		Departamento departamento = null;

		try {
			departamento = departamentoRepository.findById(cr).get();
			
			departamentoRepository.delete(departamento);

		} catch (NoSuchElementException e) {
			throw new NotFoundException("cr = " + cr);
		}

		return departamento;
	}
}

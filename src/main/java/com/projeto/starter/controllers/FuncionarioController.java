package com.projeto.starter.controllers;

import java.util.List;
import java.util.NoSuchElementException;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.projeto.starter.exceptions.NotFoundException;
import com.projeto.starter.models.Funcionario;
import com.projeto.starter.repository.FuncionarioRepository;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;

@RestController
@RequestMapping(value = "/api", produces = "application/json")
@Api(value = "funcionario")
public class FuncionarioController {

	@Autowired
	FuncionarioRepository funcionarioRepository;
	
	
	@GetMapping("/funcionarios")
	@ApiOperation(value = "Lista todos os funcionários")
	public List<Funcionario> getAllFuncionarios() {
		
		List<Funcionario> funcionario = funcionarioRepository.findAll();
		
		if (funcionario.isEmpty()) {
			throw new NotFoundException("sem cadastros");
		}
		
		return funcionario;
	}

	@GetMapping("/funcionarios/matricula/{matricula}")
	@ApiOperation(value = "Busca o funcionário pela matrícula")
	public Funcionario getFuncionarioByMatricula(@PathVariable(name = "matricula") String matricula) {

		Funcionario funcionario = null;

		try {
			funcionario = funcionarioRepository.findById(matricula).get();

		} catch (NoSuchElementException nsee) {
			throw new NotFoundException("matricula = " + matricula);
		}

		return funcionario;
	}

	@GetMapping("/funcionarios/nome/{nome}")
	@ApiOperation(value = "Lista os funcionários pelo nome")
	public List<Funcionario> getFuncionarioByName(@PathVariable(name = "nome") String nome) {

		List<Funcionario> funcionario = funcionarioRepository.findByName(nome);

		if (funcionario.isEmpty()) {
			throw new NotFoundException("nome = " + nome);
		}

		return funcionario;
	}

	@GetMapping("/funcionarios/cpf/{cpf}")
	@ApiOperation(value = "Busca o funcionario pelo CPF")
	public Funcionario getFuncionarioByCpf(@PathVariable(name = "cpf") String cpf) {

		Funcionario funcionario = funcionarioRepository.findByCpf(cpf);

		if (funcionario == null) {
			throw new NotFoundException("cpf = " + cpf);
		}

		return funcionario;
	}

	@GetMapping("/funcionarios/cr/{cr}")
	@ApiOperation(value = "Lista todos os funcionários de um departamento pelo CR")
	public List<Funcionario> getFuncionarioByDepartamento(@PathVariable(value = "cr") String cr) {
		// logger.info(nome);
		List<Funcionario> funcionario = funcionarioRepository.findByDepartamento(cr);

		if (funcionario.isEmpty()) {
			throw new NotFoundException("cr = " + cr);
		}

		return funcionario;
	}

	@PutMapping("/funcionarios/matricula/{matricula}")
	@ApiOperation(value = "Atualiza o funcionário especificado pela matrícula")
	public Funcionario updateFuncionario(@Valid @RequestBody Funcionario funcionario,
			@PathVariable(name = "matricula") String matricula) {

		Funcionario func = null;

		try {
			func = funcionarioRepository.findById(matricula).get();

			func.setCpf(funcionario.getCpf());
			func.setCrDepartamento(funcionario.getCrDepartamento());
			func.setEmail(funcionario.getEmail());
			func.setTelefone(funcionario.getTelefone());
			func.setNome(funcionario.getNome());

			func = funcionarioRepository.save(func);

		} catch (Exception e) {
			throw new NotFoundException("matricula = " + matricula);
		}

		return func;
	}

	@PostMapping("/funcionarios")
	@ApiOperation(value = "Adiciona um funcionário")
	public Funcionario addFuncionario(@Valid @RequestBody Funcionario funcionario) {
		return funcionarioRepository.save(funcionario);
	}

	@DeleteMapping("/funcionarios/matricula/{matricula}")
	@ApiOperation(value = "Deleta o funcionário especificado pela matrícula")
	public Funcionario deleteFuncionario(@PathVariable(name = "matricula") String matricula) {

		Funcionario funcionario = null;

		try {
			funcionario = funcionarioRepository.findById(matricula).get();

			funcionarioRepository.delete(funcionario);

		} catch (NoSuchElementException nsee) {
			throw new NotFoundException("matricula = " + matricula);
		}

		return funcionario;
	}
}

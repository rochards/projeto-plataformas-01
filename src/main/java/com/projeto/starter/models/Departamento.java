package com.projeto.starter.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "CT_DEPARTAMENTO", schema = "empresa")
public class Departamento {

	/*
	 * @Id -> usada para especificar a chave primária da relação
	 * @Column -> usada para mapear qual coluna estamos nos referindo na tabela
	 * @NotBlank -> usada para indicar que o valor passado não pode ser null ou simplesmente um espaço em branco
	 * @ApiModelProperty -> usada para descrever as propriedades do model
	 * */
	
	@Id
	@Column(name = "CTCDCR") 
	@ApiModelProperty(value = "CR do departamento", example = "CAP", position = 0)
	private String cr;

	@NotBlank
	@Column(name = "CTNMDEPARTAMENTO")
	@ApiModelProperty(value = "Nome do departamento", example = "Coordenação de Aplicações e Plataformas", position = 1)
	private String nome;

	@Column(name = "CTCDCOORDENADOR")
	@ApiModelProperty(value = "Matrícula do coordenador do departamento", allowEmptyValue = true, example = "CAP0000", position = 2)
	private String matriculaCoordenador;

	public Departamento() {

	}

	public Departamento(String cr, @NotBlank String nome, String matriculaCoordenador) {
		super();
		this.cr = cr;
		this.nome = nome;
		this.matriculaCoordenador = matriculaCoordenador;
	}

	public String getCr() {
		return cr;
	}

	public void setCr(String cr) {
		this.cr = cr;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getMatriculaCoordenador() {
		return matriculaCoordenador;
	}

	public void setMatriculaCoordenador(String matriculaCoordenador) {
		this.matriculaCoordenador = matriculaCoordenador;
	}
}

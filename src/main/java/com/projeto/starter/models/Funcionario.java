package com.projeto.starter.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import io.swagger.annotations.ApiModelProperty;

@Entity
@Table(name = "CT_FUNCIONARIO", schema = "empresa")
public class Funcionario {

	@Id
	@Column(name = "CTCDMATRICULA")
	@ApiModelProperty(value = "Matrícula do funcionário", example = "CAP0000", position = 0)
	private String matricula;

	@NotBlank
	@Column(name = "CTNMFUNCIONARIO")
	@ApiModelProperty(value = "nome do funcionário", position = 1)
	private String nome;

	@Column(name = "CTNMEMAIL")
	@ApiModelProperty(value = "Email do funcionário", allowEmptyValue = true, position = 2)
	private String email;

	@Column(name = "CTNUTELEFONE")
	@ApiModelProperty(value = "Telefone do funcionário", allowEmptyValue = true, example = "55034999999999", position = 3)
	private String telefone;

	@NotBlank
	@Column(name = "CTNUCPF")
	@ApiModelProperty(value = "CPF do funcionário", example = "65476153339", position = 4)
	private String cpf;

	@NotBlank
	@Column(name = "CTCDCRDEPARTAMENTO")
	@ApiModelProperty(value = "CR do departamento do funcionário", example = "CAP", position = 5)
	private String crDepartamento;

	public String getMatricula() {
		return matricula;
	}

	public void setMatricula(String matricula) {
		this.matricula = matricula;
	}

	public String getNome() {
		return nome;
	}

	public void setNome(String nome) {
		this.nome = nome;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getTelefone() {
		return telefone;
	}

	public void setTelefone(String telefone) {
		this.telefone = telefone;
	}

	public String getCpf() {
		return cpf;
	}

	public void setCpf(String cpf) {
		this.cpf = cpf;
	}

	public String getCrDepartamento() {
		return crDepartamento;
	}

	public void setCrDepartamento(String crDepartamento) {
		this.crDepartamento = crDepartamento;
	}
}

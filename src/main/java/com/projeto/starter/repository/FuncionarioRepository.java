package com.projeto.starter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.projeto.starter.models.Funcionario;

@Repository
public interface FuncionarioRepository extends JpaRepository<Funcionario, String> {

	@Query(value = "SELECT * FROM CT_FUNCIONARIO WHERE CTNMFUNCIONARIO LIKE %?1%", nativeQuery = true)
	List<Funcionario> findByName(String nome);

	@Query(value = "SELECT * FROM CT_FUNCIONARIO WHERE CTNUCPF = ?1", nativeQuery = true)
	Funcionario findByCpf(String cpf);

	@Query(value = "SELECT * FROM CT_FUNCIONARIO WHERE CTCDCRDEPARTAMENTO = ?1", nativeQuery = true)
	List<Funcionario> findByDepartamento(String cr);

}

package com.projeto.starter.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.projeto.starter.models.Departamento;

@Repository
public interface DepartamentoRepository extends JpaRepository<Departamento, String> {

	/*
	 * @Repository -> a mechanism for encapsulating storage, retrieval, and search behavior which emulates a collection of objects
	 * */
	
	@Query(value = "SELECT * FROM CT_DEPARTAMENTO WHERE CTNMDEPARTAMENTO LIKE %?1%", nativeQuery = true)
	List<Departamento> findByName(String nome);

	/*@Query(value = "SELECT * FROM CT_DEPARTAMENTO INNER JOIN CT_FUNCIONARIO ON CT_DEPARTAMENTO.CTCDCR = CT_FUNCIONARIO.CTCDCRDEPARTAMENTO AND CT_DEPARTAMENTO.CTCDCR = ?1", nativeQuery = true)
	List<Funcionario> findByDepartamento(String cr);*/

}
/*
 * You will now be able to use JpaRepository’s methods like save(), findOne(),
 * findAll(), count(), delete() etc.
 */

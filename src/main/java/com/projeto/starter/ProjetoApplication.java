package com.projeto.starter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication
@ComponentScan
public class ProjetoApplication {
	
	/* @SpringBootApplication -> anotação diz que a classe faz parte da configuração do Spring. 
	 * A anotação também define o ponto de partida para a procura dos demais componentes da aplicação.
	 * @ComponentScan -> tells Spring in which packages you have annotated classes which should be managed by Spring.
	 * */
	
	public static void main(String[] args) {
		SpringApplication.run(ProjetoApplication.class, args);
	}
}

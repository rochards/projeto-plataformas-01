USE empresa;
DESCRIBE CT_DEPARTAMENTO;
DESCRIBE CT_FUNCIONARIO;

SELECT * FROM CT_DEPARTAMENTO;
SELECT * FROM CT_FUNCIONARIO;

INSERT INTO CT_FUNCIONARIO (CTCDMATRICULA, CTNMFUNCIONARIO, CTNMEMAIL, CTNUTELEFONE, CTNUCPF, CTCDCRDEPARTAMENTO)
	VALUES
		('CAP0001', 'Rodrigo Rocha dos Santos', 'rodrigords@algartelecom.com.br', '55034999999999', '10976155439', 'CAP'),
		('CAP0002', 'José Ribeiro de Valência', 'joserdv@algartelecom.com.br', '55034999999999', '10965155437', 'CAP'),
        ('CAP0003', 'José Francisco Ribeiro', 'josefrr@algartelecom.com.br', '55034999999999', '09865355437', 'CTH'),
        ('CAP0004', 'Maria Carla de Oliveira', 'mariacdo@algartelecom.com.br', '55034999999999', '60465355437', 'CAP'),
        ('CAP0005', 'Omar Calvacante da Silva', 'omarcds@algartelecom.com.br', '55034999999999', '65465355437', 'CTH');

INSERT INTO CT_DEPARTAMENTO (CTCDCR, CTNMDEPARTAMENTO, CTCDCOORDENADOR) 
	VALUES
	('CAP', 'Coordenação de Aplicações e Plataformas', NULL),
    ('CTH', 'Coordenação de Talentos Humanos', NULL);
    
UPDATE CT_DEPARTAMENTO 
SET 
    CTCDCOORDENADOR = 'CAP0002'
WHERE
    CTCDCR = 'CAP';

SELECT * FROM CT_DEPARTAMENTO INNER JOIN CT_FUNCIONARIO ON CT_DEPARTAMENTO.CTCDCR = CT_FUNCIONARIO.CTCDCRDEPARTAMENTO AND CT_DEPARTAMENTO.CTCDCR = 'CAP';
SELECT * FROM CT_FUNCIONARIO WHERE CTCDCRDEPARTAMENTO = 'CAP';
#INSERT INTO ps_address SET
#  id_address = null,
#  id_country = @id_country,
#  id_state = @id_state,
#  id_customer = 2,